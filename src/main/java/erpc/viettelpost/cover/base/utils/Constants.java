package erpc.viettelpost.cover.base.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class Constants {
    public static final String authenKey = "Authorization";
    public enum STATUS {
        E200(200, "Success"),
        E201(201, "Token invalid"),
        E202(202, "Header invalid"),
        E203(203, "Data invalid(validate input)"),
        E204(204, "Data invalid(db raise, business error)"),
        E205(205, "Application error");
        private int code;
        private String name;

        STATUS(int code, String name) {
            this.code = code;
            this.name = name;
        }
        public int getCode() {
            return code;
        }
        public String getName() {
            return name;
        }
    }
}
