package erpc.viettelpost.cover.base.exception;

import erpc.viettelpost.cover.base.utils.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private  int code;
    private String message;
    private Object data;

    public ResourceNotFoundException(){
        super();
    }
    public ResourceNotFoundException(String message){
        this.code = 500;
        this.message = message;
    }
    public ResourceNotFoundException(int code, String message){
        this.code = code;
        this.message = message;
    }
    public ResourceNotFoundException(Constants.STATUS status, String message){
        this.code = status.getCode() ;
        this.message = message;

    }
    public int getCode() {
        return code;
    }
}
