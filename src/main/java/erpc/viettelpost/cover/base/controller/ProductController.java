package erpc.viettelpost.cover.base.controller;

import erpc.viettelpost.cover.base.exception.ResourceNotFoundException;
import erpc.viettelpost.cover.base.model.dtos.BaseResponse;
import erpc.viettelpost.cover.base.model.dtos.DataResponse;
import erpc.viettelpost.cover.base.model.entity.Product;
import erpc.viettelpost.cover.base.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/")
public class ProductController {
        @Autowired
        private ProductService productService;
        @GetMapping("/products")
        public List<Product>getProductList(@RequestParam String consumerKey) {
            log.info("Consumer: {}", consumerKey);
            return productService.findAll();
        }
        @GetMapping("/products/{productId}")
        public ResponseEntity getProduct(@PathVariable(value = "productId") Long productId){
        //return productService.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Productid:"+productId+" not found"));
            return ResponseEntity.ok(
                   new BaseResponse(false,"Get done",productService.findById(productId))
            );
        }
        @PostMapping("/products")
        public ResponseEntity createProduct(@RequestBody Product product){
            productService.save(product);
            return ResponseEntity.ok(
                    new BaseResponse(false,"Product added")
            );
        }

        @PutMapping("/products/{productId}")
        public ResponseEntity<BaseResponse> updateProduct(@PathVariable(value = "productId") Long productId, @RequestBody Product product) {
          String  error =  productService.findById(productId).map(p ->{
                p.setName(product.getName());
                p.setPrice(product.getPrice());
                productService.save(p);
                return "";
            }).orElse( "Product ID not found");
            if (!error.equals("")){
                return ResponseEntity.ok(
                        new BaseResponse(true, error)
                );
            }else {
                return ResponseEntity.ok(
                        new BaseResponse(true,"Update done")
                );
            }

        }
        @DeleteMapping("/products/{productId}")
        public ResponseEntity<BaseResponse> deleteProduct(@PathVariable(value = "productId") Long productId){
            String  error =  productService.findById(productId).map(p ->{

                productService.deleteById(productId);
                return "";
            }).orElse( "Product ID not found");
            if (!error.equals("")){
                return ResponseEntity.ok(
                        new BaseResponse(true, error)
                );
            }else {
                return ResponseEntity.ok(
                        new BaseResponse(true,"Delete done")
                );
            }
        }

}
