package erpc.viettelpost.cover.base.service;

import erpc.viettelpost.cover.base.model.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
     Product save (Product product);
     void deleteById(Long id);
     Optional<Product> findById(Long id);
     List<Product>findAll();
}
