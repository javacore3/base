package erpc.viettelpost.cover.base.service.impl;

import erpc.viettelpost.cover.base.model.entity.Product;
import erpc.viettelpost.cover.base.repository.ProductRepository;
import erpc.viettelpost.cover.base.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product save(Product product){
        return productRepository.save(product);
    }
    @Override
    public void deleteById(Long id){
        productRepository.deleteById(id);
    }
    @Override
    public Optional<Product> findById(Long id){
        return productRepository.findById(id);
    }
    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }
}
