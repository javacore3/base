package erpc.viettelpost.cover.base.repository;


import erpc.viettelpost.cover.base.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
